const express = require("express");
const app = express();
const path = require("path");
const expressLayouts = require("express-ejs-layouts");
const { detailData, loadData, saveData, cekDuplikat } = require("./utils/app");
const { body, validationResult, check } = require("express-validator");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const flash = require("connect-flash");
const port = 3000;

app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(path.join(__dirname + "/public")));
app.use(express.urlencoded({ extended: true }));

app.use(cookieParser("secret"));
app.use(session({
	secret: "secret",
	maxAge: 3600,
	resave: true,
	saveUninitialized: true
}));
app.use(flash());

app.get("/", (req, res) => {
	res.render("index", {
		layout: "layouts/base",
		title: "Home"
	});
});

app.get("/about", (req, res) => {
	res.render("about", {
		layout: "layouts/base",
		title: "About"
	});
});

app.get("/contact", (req, res) => {
	const dataContact = loadData();
	res.render("contact", {
		layout: "layouts/base",
		title: "Contact",
		dataContact,
		msg: req.flash("msg")
	});
});

app.post("/contact", [
	check("email", "Email tidak valid").isEmail(),
	body("email").custom((value) => {
		const duplikatEmail = cekDuplikat(value);
		if(duplikatEmail) {
			throw new Error("Email sudah terdaftar!");
		};
		return true;
	}),
	check("nomor", "Nomor tidak valid").isMobilePhone("id-ID")
], (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		res.render("create-data", {
			layout: "layouts/base",
			title: "Create Data",
			errors: errors.array()

		});
	} else {
		saveData(req.body);
		req.flash("msg", "Data berhasil ditambahkan");
		res.redirect("/contact");
	}
});

app.get("/contact/create/data", (req, res) => {
	res.render("create-data", {
		layout: "layouts/base",
		title: "Create Data"
	});
});

app.get("/contact/:id", (req, res) => {
	const id = req.params.id;
	const contact = detailData(id);
	res.render("detail", {
		layout: "layouts/base",
		title: "Detail",
		contact
	});
});

app.use("/", (req, res) => {
	res.status(404);
	res.send("404 not found");
});

app.listen(port, () => console.log(`Server listen at http://localhost:3000`));
