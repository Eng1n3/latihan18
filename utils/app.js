const fs = require("fs");
const validator = require("validator");

const dirPath = "./data";
if (!fs.existsSync(dirPath)) fs.mkdirSync(dirPath);

const dirFile = "./data/db.json";
if (!fs.existsSync(dirFile)) fs.writeFileSync(dirFile, "[]", "utf8");

const loadData = () => {
	const contacts = JSON.parse(fs.readFileSync(dirFile));
	return contacts
};

//const saveData = ( nama, email, nomor ) => {
//	const slug = nama.toLowerCase().replace(" ", "-");
//	const dataContact = { nama, email, nomor, slug };
//	const data = loadData();
//	if (!validator.isEmail(email)) return false;
//	if (!validator.isMobilePhone(nomor, "id-ID")) return false;
//	const duplikatEmail = data.find(contact => contact.email === email);
//	if (duplikatEmail) return false;
//	data.push(dataContact);
//	fs.writeFileSync(dirFile, JSON.stringify(data), "utf8");
//};

const addData = (value) => {
	fs.writeFileSync(dirFile, JSON.stringify(value), "utf8");
};

const saveData = (value) => {
	const data = loadData();
	let id = "1";
	if (data.length > 0) {
		id = `${parseInt(data[data.length - 1].id) + 1}`;
	}
	console.log(typeof id);
	const newData = { id, ...value };
	data.push(newData);
	addData(data);
};

const detailData = id => {
	const data = loadData();
	const contact = data.find(contact => contact.id === id);
	console.log(typeof id);
	return contact
};

const cekDuplikat = value => {
	const data = loadData();
	return data.find(contact => contact.email === value);
}

module.exports = {
	loadData,
	detailData,
	saveData,
	cekDuplikat
};
